const express = require("express");

const router = express.Router();
const controller = require("../controller");
const userController = require("../controller/user");
const regionController = require("../controller/region");
const divisionController = require("../controller/division");
const categoryController = require("../controller/category");
const scoreController = require("../controller/score");
const leaderboardController = require("../controller/leaderboard");
const questionController = require("../controller/question");
const badgeController = require("../controller/badge");

router
  .route("/login")
  .get(controller.login)
  .post(controller.doLogin);

router.get("/verifytoken", controller.verifySsoToken);

router.get("/user", userController.getUser);
router.get("/users", userController.getUsers);
router.post("/login_user", controller.userLogin);
router.post("/user", userController.registerUser);

router.post("/origin", userController.registerOrigin);
router.post("/region", regionController.registerRegion);
router.post("/division", divisionController.registerDivision);

router.post('/score', scoreController.setScore);
router.get('/score', scoreController.getScore);
router.get('/leaderboard', leaderboardController.getLeaderboard);

router.post("/category", categoryController.registerCategory);
router.get('/category', categoryController.getCategories);

router.get('/question', questionController.getQuestions);
router.post('/question', questionController.registerQuestion);
router.put('/question/:id', questionController.editQuestion);
router.post('/answer_question', questionController.answerQuestion);

router.post('/badge', badgeController.registerBadge);
router.get('/badge', badgeController.getBadge);

module.exports = router;
