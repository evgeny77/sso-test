var mongoose = require('mongoose');
var User = mongoose.model('User');
var Category = mongoose.model('Category');
var Question = mongoose.model('Question');
const userController = require("./user");
const badgeController = require("./badge");

const getQuestions = (req, res, next) => {
  Question.find({}).then((questions) => {
    return res.status(200).json({questions: questions.map((question) => {return question.toJSON();})});
  }).catch(next);
}

const registerQuestion = async (req, res, next) => {
  category = await Category.findOne({name: req.body.category});
  question = new Question();
  question.date = req.body.date;
  question.category = req.body.category;
  question.color = (req.body.category ? req.body.category : null);
  question.title = req.body.title;
  question.answers = req.body.answers;
  question.answer_correct = req.body.answer_correct;
  question.explanation = req.body.explanation;

  await question.save();
  return res.status(200).json({question: question.toJSON()});
}

const editQuestion = async (req, res, next) => {
  category = await Category.findOne({name: req.body.category});
  if(req.params.id === -1) {
    question = new Question();
  }
  else {
    question = await Question.findOne({_id: req.params.id});
    if(!question) {
      question = new Question();
    }
  }
  question.date = req.body.date;
  question.category = req.body.category;
  question.color = (req.body.category ? req.body.category : null);
  question.title = req.body.title;
  question.answers = req.body.answers;
  question.answer_correct = req.body.answer_correct;
  question.explanation = req.body.explanation;

  await question.save();
  return res.status(200).json({question: question.toJSON()});
}

const answerQuestion = async (req, res, next) => {
  User.findOne({email: req.body.email}).then(async function(user){
    if(!user) {
      return res.status(404).json({errors: 'User does not exist'});
    }

    stat = {};
    correct = parseInt(req.body.correct);
    if(!user.stat.hasOwnProperty(req.body.category)) {
      stat[req.body.category] = correct;
    } else {
      stat[req.body.category] = user.stat[req.body.category] + correct;
    }
    stat.question = user.stat.question + 1;
    stat.question_correct = user.stat.question_correct + correct;
    stat.rank = await User.count({score: {'$gt': user.score}}) + 1;
    if(correct) {
      stat.first_date_campaign = user.stat.first_date_campaign;
      if(stat.first_date_campaign === '') {
        stat.first_date_campaign = new Date().toISOString().slice(0,10);
      }
    }

    user.stat = {
      ...user.stat,
      ...stat,
    }

    user = userController.updateUserTime(user);
    user.badge = await badgeController.updateBadge(user);
    await user.save();

    return res.status(200).json({
      success: 'success'
    });
  }).catch(next);
};

module.exports = Object.assign({}, { getQuestions, registerQuestion, editQuestion, answerQuestion });