var mongoose = require('mongoose');
var Region = mongoose.model('Region');

const registerRegion = (req, res, next) => {
  var region = new Region();
  region.name = req.body.name;
  region.score = 0;

  region.save().then(function(){
    return res.status(200).json(region.toJSON());
  }).catch(next);
};

module.exports = Object.assign({}, { registerRegion });