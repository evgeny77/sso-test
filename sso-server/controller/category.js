var mongoose = require('mongoose');
var Category = mongoose.model('Category');

const registerCategory = (req, res, next) => {
  var category = new Category();
  category.name = req.body.name;
  category.color = req.body.color;

  category.save().then(function(){
    return res.status(200).json(category.toJSON());
  }).catch(next);
};

const getCategories = (req, res, next) => {
  Category.find({}).then((categories) => {
    return res.status(200).json({categories: categories.map((category) => {return category.toJSON();})});
  }).catch(next);
}

module.exports = Object.assign({}, { registerCategory, getCategories });