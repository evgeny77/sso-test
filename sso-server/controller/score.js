var mongoose = require('mongoose');
var User = mongoose.model('User');
var Region = mongoose.model('Region');
var Division = mongoose.model('Division');
const userController = require("./user");
const badgeController = require("./badge");

const setScore = async (req, res, next) => {
  User.findOne({email: req.body.email}).then(async function(user){
    if(!user) {
      return res.status(404).json({errors: 'User does not exist'});
    }
    user.score = req.body.score;
    rank = await User.count({score: {'$gt': req.body.score}}) + 1;
    first_date_played = user.stat.first_date_played;
    if(first_date_played === '') {
      first_date_played = new Date().toISOString().slice(0,10);
    }
    last_date_played = new Date().toISOString().slice(0,10);
    days_played = user.stat.days_played;
    if(last_date_played !== user.stat.last_date_played) {
      days_played += 1;
    }

    user.stat = {
      ...user.stat,
      play: user.stat.play + 1,
      rank: rank,
      first_date_played: first_date_played,
      last_date_played: last_date_played,
      days_played: days_played,
    };

    user = userController.updateUserTime(user);
    user.badge = await badgeController.updateBadge(user);
    await user.save();

    // Region
    region = await Region.findOne({name: user.region});
    search_option = {region: user.region};
    users = await User.find(search_option);
    score_region = 0;
    users.map((item, idx) => {
      score_region += item.score;
    });
    region.score = score_region;
    await region.save();

    // Division
    division = await Division.findOne({name: user.division});
    search_option = {division: user.division};
    users = await User.find(search_option);
    score_division = 0;
    users.map((item) => {
      score_division += item.score;
    });
    division.score = score_division;
    await division.save();
    
    return res.status(200).json({
      email: user.email,
      score: user.score,
    });
  }).catch(next);
};

const getScore = (req, res, next) => {
  User.findOne({email: req.query.email}).then(async function(user){
    if(!user) {
      return res.status(404).json({errors: 'User does not exist'});
    }

    user = userController.updateUserTime(user);
    await user.save();

    // console.log(user);

    region = await Region.findOne({name: user.region});
    division = await Division.findOne({name: user.division});
    
    return res.status(200).json({
      email: user.email,
      score: user.score,
      score_region: region.score,
      score_division: division.score,
    });
  }).catch(next);
}

module.exports = Object.assign({}, { setScore, getScore });