var mongoose = require('mongoose');
var Region = mongoose.model('Region');
var Division = mongoose.model('Division');

const getLeaderboard = async (req, res, next) => {
  regions = await Region.find().sort({score: -1});
  divisions = await Division.find().sort({score: -1});

  return res.status(200).json({
    regions: regions.map((item) => item.toJSON()),
    divisions: divisions.map((item) => item.toJSON()),
  })    
}

module.exports = Object.assign({}, { getLeaderboard });