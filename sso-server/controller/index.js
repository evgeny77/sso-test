var mongoose = require('mongoose');
var User = mongoose.model('User');
var Origin = mongoose.model('Origin');
const userController = require("./user");
const URL = require("url").URL;
const { genJwtToken } = require("./jwt_helper");

const re = /(\S+)\s+(\S+)/;

// Note: express http converts all headers
// to lower case.
const AUTH_HEADER = "authorization";
const BEARER_AUTH_SCHEME = "bearer";

function parseAuthHeader(hdrValue) {
  if (typeof hdrValue !== "string") {
    return null;
  }
  const matches = hdrValue.match(re);
  return matches && { scheme: matches[1], value: matches[2] };
}

const fromAuthHeaderWithScheme = function(authScheme) {
  const authSchemeLower = authScheme.toLowerCase();
  return function(request) {
    let token = null;
    if (request.headers[AUTH_HEADER]) {
      const authParams = parseAuthHeader(request.headers[AUTH_HEADER]);
      if (authParams && authSchemeLower === authParams.scheme.toLowerCase()) {
        token = authParams.value;
      }
    }
    return token;
  };
};

const fromAuthHeaderAsBearerToken = function() {
  return fromAuthHeaderWithScheme(BEARER_AUTH_SCHEME);
};

const appTokenFromRequest = fromAuthHeaderAsBearerToken();

const sessionUser = {};
const sessionApp = {};

// these token are for the validation purpose
const intrmTokenCache = {};

const fillIntrmTokenCache = (originAppName, id, intrmToken) => {
  intrmTokenCache[intrmToken] = [id, originAppName];
};
const storeApplicationInCache = (url, id, intrmToken) => {
  Origin.findOne({url: url}).then(function(origin) {
    const appName = origin.app_name;
    if (sessionApp[id] == null) {
      sessionApp[id] = {
        [appName]: true
      };
      fillIntrmTokenCache(appName, id, intrmToken);
    } else {
      sessionApp[id][appName] = true;
      fillIntrmTokenCache(appName, id, intrmToken);
    }
    // console.log({ ...sessionApp }, { ...sessionUser }, { intrmTokenCache });
  });
};

const generatePayload = async ssoToken => {
  const globalSessionToken = intrmTokenCache[ssoToken][0];
  const appName = intrmTokenCache[ssoToken][1];
  const userEmail = sessionUser[globalSessionToken];
  const user = await User.findOne({email: userEmail});
  if(user) {
    const appPolicy = user.app_policy[appName];
    const email = appPolicy.share_email == true ? userEmail : undefined;
    const payload = {
      email: user.email,
      name: user.name,
      score: user.score,
      // global SessionID for the logout functionality.
      globalSessionID: globalSessionToken
    };
    console.log(JSON.stringify(payload));
    return payload;
  }
};

const verifySsoToken = async (req, res, next) => {
  const appToken = appTokenFromRequest(req);
  const { ssoToken } = req.query;
  // if the application token is not present or ssoToken request is invalid
  // if the ssoToken is not present in the cache some is
  // smart.
  if (
    appToken == null ||
    ssoToken == null ||
    intrmTokenCache[ssoToken] == null
  ) {
    return res.status(400).json({ message: "badRequest" });
  }

  // if the appToken is present and check if it's valid for the application
  const appName = intrmTokenCache[ssoToken][1];
  const globalSessionToken = intrmTokenCache[ssoToken][0];
  // If the appToken is not equal to token given during the sso app registraion or later stage than invalid
  origin = await Origin.findOne({app_name: appName});
  if(!origin) {
    return res
      .status(400)
      .json({ message: "You are not allowed to access the sso-server" });
  }

  if (
    appToken !== origin.app_token ||
    sessionApp[globalSessionToken][appName] !== true
  ) {
    return res.status(403).json({ message: "Unauthorized" });
  }
  // checking if the token passed has been generated
  const payload = await generatePayload(ssoToken);

  console.log(JSON.stringify(payload));

  const token = await genJwtToken(payload);
  // delete the itremCache key for no futher use,
  delete intrmTokenCache[ssoToken];
  return res.status(200).json({ token });
};
const doLogin = (req, res, next) => {
  // do the validation with email and password
  // but the goal is not to do the same in this right now,
  // like checking with Datebase and all, we are skiping these section
  const { email, password } = req.body;

  User.findOne({email: email, password: password}).then(function(user) {
    if(!user) {
      return res.status(404).json({ message: "Invalid email and password" });
    }
    // else redirect
    const { serviceURL } = req.query;
    const id = user.user_id;
    req.session.user = id;
    sessionUser[id] = email;
    if (serviceURL == null) {
      return res.redirect("/");
    }
    const url = new URL(serviceURL);
    const intrmid = id;
    Origin.findOne({url: url}).then(function(origin) {
      if(!origin) {
        return res
          .status(400)
          .json({ message: "You are not allowed to access the sso-server" });
      }
      storeApplicationInCache(url, id, intrmid);
      return res.redirect(`${serviceURL}?ssoToken=${intrmid}`);
    }).catch(next);
  }).catch(next);
};

const login = async (req, res, next) => {
  // The req.query will have the redirect url where we need to redirect after successful
  // login and with sso token.
  // This can also be used to verify the origin from where the request has came in
  // for the redirection
  const { serviceURL } = req.query;
  // direct access will give the error inside new URL.
  if (serviceURL != null) {
    const url = new URL(serviceURL);
    origin = await Origin.findOne({url: url, allow: true});
    if(!origin) {
      return res
        .status(400)
        .json({ message: "You are not allowed to access the sso-server" });
    }
  }

  if (req.session.user != null && serviceURL == null) {
    return res.redirect("/");
  }
  // if global session already has the user directly redirect with the token
  if (req.session.user != null && serviceURL != null) {
    const url = new URL(serviceURL);
    const intrmid = req.session.user;
    storeApplicationInCache(url, req.session.user, intrmid);
    return res.redirect(`${serviceURL}?ssoToken=${intrmid}`);
  }

  return res.render("login", {
    title: "SSO-Server | Login"
  });
};

const userLogin = (req, res, next) => {
  const { email, password } = req.body;

  User.findOne({email: email.toLowerCase(), password: password}).then(async function(user) {
    if(!user) {
      return res.status(422).json({errors: { invalidCredentials: 'Invalid Email or Password' }});
    }

    user = userController.updateUserTime(user);
    await user.save();

    return res.status(200).json({ user: user.toJSON() });
  }).catch(next);
};

module.exports = Object.assign({}, { doLogin, login, verifySsoToken, userLogin });
