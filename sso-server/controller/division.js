var mongoose = require('mongoose');
var Division = mongoose.model('Division');

const registerDivision = (req, res, next) => {
  var division = new Division();
  division.name = req.body.name;
  division.score = 0;

  division.save().then(function(){
    return res.status(200).json(division.toJSON());
  }).catch(next);
};

module.exports = Object.assign({}, { registerDivision });