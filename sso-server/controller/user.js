var mongoose = require('mongoose');
var User = mongoose.model('User');
var Origin = mongoose.model('Origin');
const uuidv4 = require("uuid/v4");
const Hashids = require("hashids");
const hashids = new Hashids();

const deHyphenatedUUID = () => uuidv4().replace(/-/gi, "");
const encodedId = () => hashids.encodeHex(deHyphenatedUUID());

const registerUser = async (req, res, next) => {
  var user = new User();
  user.email = req.body.email.toLowerCase();
  user.password = req.body.password;
  user.user_id = encodedId();
  user.first_name = req.body.first_name;
  user.last_name = req.body.last_name;
  user.department = req.body.department;
  user.state = req.body.state;
  user.city = req.body.city;
  user.division = req.body.division;
  user.region = req.body.region;
  user.employee_status = req.body.employee_status;
  user.nt_id = req.body.nt_id;

  user.score = req.body.score;
  user.time = 0;

  rank = await User.count({score: {'$gt': 0}}) + 1;

  user.stat = {
    play: 0,
    question: 0,
    question_correct: 0,
    first_date_campaign: '',
    first_date_played: '',
    last_date_played: '',
    days_played: 0,
    rank: rank,
    sound_muted: false,
  };
  user.badge = [];
  user.app_policy = req.body.app_policy;

  user.save().then(function(){
    return res.status(200).json(user.toJSON());
  }).catch(next);
};

const registerOrigin = (req, res, next) => {
  var origin = new Origin();
  origin.url = req.body.url;
  origin.allow = req.body.allow;
  origin.app_name = req.body.app_name;
  origin.app_token = req.body.app_token;

  origin.save().then(function(){
    return res.status(200).json(origin.toJSON());
  }).catch(next);
};

const getUser = (req, res, next) => {
  User.findOne({email: req.query.email}).then(async function(user){
    if(!user) {
      return res.status(404).json({errors: 'User does not exist'});
    }

    user = updateUserTime(user);
    await user.save();

    return res.status(200).json(user);
  }).catch(next);
}

const getUsers = (req, res, next) => {
  User.find({}).then(function(users){
    return res.status(200).json({users: users.map((user) => {
      return user.toJSON();
    })});
  }).catch(next);
}

const updateUserTime = (user) => {
  var updatedAt = new Date(user.updatedAt);
  var time = new Date() - updatedAt;
  if(time < 10 * 60 * 1000) { 
    // less than 10 mins 
    user.time += time;
  }
  return user;
}

module.exports = Object.assign({}, { registerUser, registerOrigin, getUser, getUsers, updateUserTime });