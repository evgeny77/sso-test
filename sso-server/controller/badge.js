var mongoose = require('mongoose');
var User = mongoose.model('User');
var Badge = mongoose.model('Badge');
const userController = require("./user");

const registerBadge = (req, res, next) => {
  var badge = new Badge();
  badge.id = req.body.id;
  badge.name = req.body.name;
  badge.description = req.body.description;
  badge.type = req.body.type;
  badge.value = req.body.value;

  badge.save().then(function(){
    return res.status(200).json(badge.toJSON());
  }).catch(next);
};

const getBadge = async (req, res, next) => {
  User.findOne({email: req.query.email}).then(async function(user){
    if(!user) {
      return res.status(404).json({errors: 'User does not exist'});
    }

    user = userController.updateUserTime(user);
    // console.log(user);
    user.badge = await updateBadge(user);
    // console.log(user);

    await user.save();
    
    return res.status(200).json({
      badges: user.badge
    });
  }).catch(next);
}

const updateBadge = async (user) => {
  badges = await Badge.find({});
  badge = [];
  badges.map((item) => {
    if(!badge.includes(item.id)) {
      if(item.type == 'score') {
        if(item.value <= user.score) {
          badge.push(item.id);
        }
      } else if(user.stat.hasOwnProperty(item.type)) {
        if(item.value <= user.stat[item.type]) {
          badge.push(item.id);
        }
      }
    }
  });

  return badge;
}

module.exports = Object.assign({}, { registerBadge, getBadge, updateBadge });