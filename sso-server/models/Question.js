var mongoose = require('mongoose');
var SchemaTypes = mongoose.Schema.Types;

var QuestionSchema = new mongoose.Schema({
  date: SchemaTypes.Date,
  category: String,
  color: String,
  title: String,
  answers: SchemaTypes.Mixed,
  answer_correct: SchemaTypes.Number,
  explanation: String,
});

QuestionSchema.methods.toJSON = function(){
  return {
    _id: this._id,
    date: this.date,
    start: this.date,
    end: this.date,
    category: this.category,
    color: this.color,
    title: this.title,
    answers: this.answers,
    answer_correct: this.answer_correct,
    explanation: this.explanation,
    allDay: true,
  };
};

mongoose.model('Question', QuestionSchema, 'question');