var mongoose = require('mongoose');
var SchemaTypes = mongoose.Schema.Types;

var UserSchema = new mongoose.Schema({
  email: String,
  password: String,
  user_id: String,
  first_name: String,
  last_name: String,
  department: SchemaTypes.Number,
  state: String,
  city: String,
  division: String,
  region: String,
  employee_status: String,
  nt_id: String,

  score: SchemaTypes.Number,
  time: SchemaTypes.Number,
  stat: SchemaTypes.Mixed,
  badge: SchemaTypes.Mixed,
  app_policy: SchemaTypes.Mixed,
}, {timestamps: true});

UserSchema.methods.toJSON = function(){
  return {
    email: this.email,
    user_id: this.user_id,
    first_name: this.first_name,
    last_name: this.last_name,
    department: this.department,
    state: this.state,
    city: this.city,
    division: this.division,
    region: this.region,
    employee_status: this.employee_status,
    nt_id: this.nt_id,

    score: this.score,
    time: this.time,
    stat: this.stat, 
    badge: this.badge,
    app_policy: this.app_policy,

    updatedAt: this.updatedAt,
    createdAt: this.createdAt,
  };
};

mongoose.model('User', UserSchema, 'user');