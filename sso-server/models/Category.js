var mongoose = require('mongoose');

var CategorySchema = new mongoose.Schema({
  name: String,
  color: String,
});

CategorySchema.methods.toJSON = function(){
  return {
    _id: this._id,
    name: this.name,
    color: this.color,
  };
};

mongoose.model('Category', CategorySchema, 'category');