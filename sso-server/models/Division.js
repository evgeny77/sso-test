var mongoose = require('mongoose');
var SchemaTypes = mongoose.Schema.Types;

var DivisionSchema = new mongoose.Schema({
  name: String,
  score: SchemaTypes.Number,
});

DivisionSchema.methods.toJSON = function(){
  return {
    name: this.name,
    score: this.score,
  };
};

mongoose.model('Division', DivisionSchema, 'division');