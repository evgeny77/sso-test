var mongoose = require('mongoose');
var SchemaTypes = mongoose.Schema.Types;

var BadgeSchema = new mongoose.Schema({
  id: SchemaTypes.Number,
  name: String,
  description: String,
  type: String,
  value: SchemaTypes.Number,
});

BadgeSchema.methods.toJSON = function(){
  return {
    id: this.id,
    name: this.name,
    description: this.description,
    type: this.type,
    value: this.value,
  };
};

mongoose.model('Badge', BadgeSchema, 'badge');