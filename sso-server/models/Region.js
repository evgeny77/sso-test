var mongoose = require('mongoose');
var SchemaTypes = mongoose.Schema.Types;

var RegionSchema = new mongoose.Schema({
  name: String,
  score: SchemaTypes.Number,
});

RegionSchema.methods.toJSON = function(){
  return {
    name: this.name,
    score: this.score,
  };
};

mongoose.model('Region', RegionSchema, 'region');