var mongoose = require('mongoose');

var OriginSchema = new mongoose.Schema({
  url: String,
  allow: Boolean,
  app_name: String,
  app_token: String,
});

OriginSchema.methods.toJSON = function(){
  return {
    url: this.url,
    allow: this.allow,
    app_name: this.app_name,
    app_token: this.app_token,
  };
};

mongoose.model('Origin', OriginSchema, 'origin');