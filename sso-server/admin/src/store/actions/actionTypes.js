export const LOGIN_SUCCESSFUL = 'LOGIN_SUCCESSFUL';
export const REGISTER_SUCCESSFUL = 'REGISTER_SUCCESSFUL';
export const LOGOUT_USER = 'LOGOUT_USER';

export const GET_USERS = 'GET_USERS';
export const GET_CATEGORIES = 'GET_CATEGORIES';
export const GET_QUESTIONS = 'GET_QUESTIONS';
export const ADD_QUESTION = 'ADD_QUESTION';
export const EDIT_QUESTION = 'EDIT_QUESTION';
export const DELETE_QUESTION = 'DELETE_QUESTION';