import * as actionTypes from './actionTypes'
import { URL } from '../../variables/global';

const options = (method, data) => {
  return {
    headers: {
      'Content-Type': 'application/json',
    },
    method: method,
    body: JSON.stringify(data)
  };
};

export const getUsersRequest = () => {
  return dispatch => {
    return fetch(URL + '/users', options('get'))
    .then(res => res.json())
    .then(res => {
      dispatch({
        type: actionTypes.GET_USERS,
        users: res.users,
      });
      return res;
    });
  }
}

export const getCategoriesRequest = () => {
  return dispatch => {
    return fetch(URL + '/category', options('get'))
    .then(res => res.json())
    .then(res => {
      dispatch({
        type: actionTypes.GET_CATEGORIES,
        categories: res.categories,
      });
      return res;
    });
  }
}

export const getQuestionsRequest = () => {
  return dispatch => {
    return fetch(URL + '/question', options('get'))
    .then(res => res.json())
    .then(res => {
      dispatch({
        type: actionTypes.GET_QUESTIONS,
        questions: res.questions,
      });
      return res;
    });
  }
}

export const addQuestionRequest = (data) => {
  return dispatch => {
    return fetch(URL + '/question', options('post', data))
    .then(res => res.json())
    .then(res => {
      dispatch({
        type: actionTypes.ADD_QUESTION,
        question: res.question,
      });
      return res;
    });
  }
}

export const editQuestionRequest = (data) => {
  return dispatch => {
    return fetch(URL + '/question/' + data._id, options('put', data))
    .then(res => res.json())
    .then(res => {
      dispatch({
        type: actionTypes.EDIT_QUESTION,
        question: res.question,
      });
      return res;
    });
  }
}

export const deleteQuestionRequest = (data) => {
  return dispatch => {
    return fetch(URL + '/question/' + data._id, options('delete', data))
    .then(res => res.json())
    .then(res => {
      return res;
    });
  }
}
