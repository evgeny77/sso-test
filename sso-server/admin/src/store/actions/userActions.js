import * as actionTypes from './actionTypes'
import { URL } from '../../variables/global';

const options = data => {
  return {
    headers: {
      'Content-Type': 'application/json'
    },
    method: 'post',
    body: JSON.stringify(data)
  };
};

export const userRegisterRequest = (userSignupDetails) => {
  return dispatch => {
    return fetch(URL + '/register', options(userSignupDetails))
    .then(res => res.json());
  }
}

export const userLoginRequest = (userLoginDetails) => {
  return dispatch => {
    return fetch(URL + '/login_user', options(userLoginDetails))
    .then(res => res.json())
    .then(res => {
      if (!res.errors) {
        localStorage.setItem('email', res.user.email);
        localStorage.setItem('username', res.user.name);
        dispatch({ 
          type: actionTypes.LOGIN_SUCCESSFUL, 
          authorizationEmail: res.user.email, 
          authenticatedUsername: res.user.name 
        });
      }
      return res;
    })
  }   
}

export const userLogoutRequest = () => {
    return dispatch => {
        localStorage.removeItem('email');
        localStorage.removeItem('username');
        dispatch({ type: actionTypes.LOGOUT_USER });
    }
}