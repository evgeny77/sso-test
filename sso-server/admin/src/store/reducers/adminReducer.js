import * as actionTypes from '../actions/actionTypes';

const initialState = {
  users: [],
  categories: [],
  questions: [],
  errors: {},
};

const reducer = (state = initialState, action) => {
  var questions;
  switch (action.type) {
    case actionTypes.GET_USERS: {
      console.log(action.users);
      return {
        ...state,
        users: action.users,
      };
    }
    case actionTypes.GET_CATEGORIES: {
      return {
        ...state,
        categories: action.categories,
      };
    }
    case actionTypes.GET_QUESTIONS: {
      return {
        ...state,
        questions: action.questions,
      };
    }
    case actionTypes.ADD_QUESTION: {
      questions = state.questions;
      questions.push(action.question);
      return {
        ...state,
        questions: questions,
      };
    }
    case actionTypes.EDIT_QUESTION: {
      questions = state.questions;
      questions.map((question, idx) => {
        if(question._id === action.question._id) {
          questions[idx] = action.question;
        }
      })
      return {
        ...state,
        questions: questions,
      };
    }
    default: {
      return state;
    }
  }
}

export default reducer;