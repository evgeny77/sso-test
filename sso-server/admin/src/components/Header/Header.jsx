import React from "react";
import PropTypes from "prop-types";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";

// material-ui icons
import Menu from "@material-ui/icons/Menu";

// core components
// import HeaderLinks from "./HeaderLinks";
import Button from "../CustomButtons/Button.jsx";

import headerStyle from "../../assets/jss/material-dashboard-pro-react/components/headerStyle.jsx";

function Header({ ...props }) {
  const { classes } = props;
  
  return (
    <header className="header">
      <div className="mobile_toggle">
        <Button
          className={classes.appResponsive}
          color="transparent"
          justIcon
          aria-label="open drawer"
          onClick={props.handleDrawerToggle}
        >
          <Menu />
        </Button>
      </div>
      <img src={props.splashLogo} alt="" />
    </header>
  );
}

Header.propTypes = {
  classes: PropTypes.object.isRequired,
  color: PropTypes.oneOf(["primary", "info", "success", "warning", "danger"]),
  rtlActive: PropTypes.bool
};

export default withStyles(headerStyle)(Header);
