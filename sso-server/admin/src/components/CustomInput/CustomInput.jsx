import React from "react";
// nodejs library to set properties for components
import PropTypes from "prop-types";
// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import FormControl from "@material-ui/core/FormControl";
import FormHelperText from "@material-ui/core/FormHelperText";

import customInputStyle from "../../assets/jss/material-dashboard-pro-react/components/customInputStyle.jsx";

class CustomInput extends React.Component {
  onChangeField(event) {
    this.props.onChange(event);
  }
  render() {
    const {
      classes,
      labelClass,
      formControlProps,
      labelText,
      id,
      error,
      white,
      inputRootCustomClasses,
      success,
      helpText,
      style,
      value
    } = this.props;
    const labelClasses = classNames({
      [" " + classes.labelRootError]: error,
      [" " + classes.labelRootSuccess]: success && !error
    });
    var formControlClasses;
    if (formControlProps !== undefined) {
      formControlClasses = classNames(
        formControlProps.className,
        classes.formControl
      );
    } else {
      formControlClasses = classes.formControl;
    }
    var helpTextClasses = classNames({
      [classes.labelRootError]: error,
      [classes.labelRootSuccess]: success && !error
    });
    return (
      <FormControl {...formControlProps} className={formControlClasses} style={style}>
        {labelText !== undefined ? (
          <label className={labelClass ? labelClass : 'event-label'} htmlFor={id}>{labelText}</label>
        ) : null}
        <input type="text" className="form-control" id={id} onChange={this.onChangeField.bind(this)} defaultValue={value} />
        {(helpText !== undefined || helpText !== null || helpText !== '') ? (
          <FormHelperText id={id + "-text"} className={'form_help_error ' + helpTextClasses}>
            {helpText}
          </FormHelperText>
        ) : null}
      </FormControl>
    );
  }
}

CustomInput.propTypes = {
  classes: PropTypes.object.isRequired,
  labelText: PropTypes.node,
  labelProps: PropTypes.object,
  id: PropTypes.string,
  inputProps: PropTypes.object,
  formControlProps: PropTypes.object,
  inputRootCustomClasses: PropTypes.string,
  error: PropTypes.bool,
  success: PropTypes.bool,
  white: PropTypes.bool,
  helpText: PropTypes.node,
  style: PropTypes.object,
};

export default withStyles(customInputStyle)(CustomInput);
