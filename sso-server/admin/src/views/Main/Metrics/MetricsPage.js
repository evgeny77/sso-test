import React from "react";
import Select from 'react-select';
import { connect } from 'react-redux';
import { getUsersRequest } from '../../../store/actions/adminActions';
import Donut from 'react-svg-donuts';
import moment from 'moment';
import DateRangePicker from 'react-bootstrap-daterangepicker';
import 'bootstrap-daterangepicker/daterangepicker.css';

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";

// core components
import buttonStyle from "assets/jss/material-dashboard-pro-react/components/buttonStyle.jsx";
import regularFormsStyle from "../../../assets/jss/material-dashboard-pro-react/views/regularFormsStyle";

import icon1 from "../../../assets/img/cyber/badge/icon-1.png";
import icon2 from "../../../assets/img/cyber/badge/icon-2.png";
import icon3 from "../../../assets/img/cyber/badge/icon-3.png";
import icon4 from "../../../assets/img/cyber/badge/icon-4.png";
import icon5 from "../../../assets/img/cyber/badge/icon-5.png";
import icon6 from "../../../assets/img/cyber/badge/icon-6.png";
import icon7 from "../../../assets/img/cyber/badge/icon-7.png";
import icon8 from "../../../assets/img/cyber/badge/icon-8.png";
import icon9 from "../../../assets/img/cyber/badge/icon-9.png";
import icon10 from "../../../assets/img/cyber/badge/icon-10.png";
import icon11 from "../../../assets/img/cyber/badge/icon-11.png";
import icon12 from "../../../assets/img/cyber/badge/icon-12.png";

import './donut.css';
 
// The value will be wrapped inside a <strong> tag.
// const renderProgress = progress => <strong>{progress}%</strong>;

class MetricsPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      new_users: 0,
      online_users: 0,
      avg_currency: 0,
      avg_time: 0,
      accuracy: 0,
      accuracy_user: 'all',
      badge_user: 'all',
      badges: [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
    };
  }
  async componentDidMount() {
    await this.props.getUsersRequest();

    var new_users = 0, online_users = 0, avg_currency = 0, avg_time = 0;
    this.props.users.map((user) => {
      var curDate = new Date();
      var updatedAt = new Date(user.updatedAt);
      var createdAt = new Date(user.createdAt);
      if(curDate - createdAt < 1 * 24 * 60 * 60 * 1000) {
        new_users += 1;
      }
      if(curDate - updatedAt < 10 * 60 * 1000) {
        online_users += 1;
      }
      avg_currency += user.score;
      avg_time += user.time;
    })
    if(this.props.users.length > 0) {
      avg_currency /= this.props.users.length;
      avg_time /= this.props.users.length;
    }
    this.setState({
      new_users: new_users,
      online_users: online_users,
      avg_currency: avg_currency,
      avg_time: avg_time,
    })

    this.updateAccuracy(this.state.accuracy_user);
    this.updateBadge(this.state.badge_user);
  }
  updateAccuracy = (accuracy_user) => {
    var acc = 0
    this.props.users.map((user) => {
      if(accuracy_user === 'all' || accuracy_user === user.email) {
        if(user.stat.question > 0) {
          acc += (user.stat.question_correct / user.stat.question) * 100;
        }
      }
    })
    if(accuracy_user === 'all' && this.props.users.length > 0) {
      acc /= this.props.users.length;
    }
    this.setState({
      accuracy_user: accuracy_user,
      accuracy: acc,
    })
  }
  updateBadge = (badge_user) => {
    var badges = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
    this.props.users.map((user) => {
      if(badge_user === 'all' || badge_user === user.email) {
        user.badge.map((id) => {
          badges[id] += 1;
        })
      }
    })
    this.setState({
      badge_user: badge_user,
      badges: badges,
    })
  }
  handleDropdownChangeBadge = (e) => {
    const value = e.value;
    this.updateBadge(value);
  }
  handleDropdownChangeAccuracy = (e) => {
    const value = e.value;
    this.updateAccuracy(value);
  }
  optionsTotal() {
    let options = this.props.users.map((user) => {
      return {
        value: user.email,
        label: user.email
      }
    });
    options.unshift({
      value: 'all',
      label: 'All Employees'
    });
    return options;
  }
  handleDateRangePickerEvent(event, picker) {
    document.querySelector('.datepicker span').innerText = picker.startDate.format('MMMM D, YYYY') + ' - ' + picker.endDate.format('MMMM D, YYYY');
  }
  render() {
    const exportDatepickerStart = moment().subtract(29, 'days');
    const exportDatepickerEnd = moment();
    const exportDatepickerStartFormat = exportDatepickerStart.format('MMMM D, YYYY'); 
    const exportDatepickerEndFormat = exportDatepickerEnd.format('MMMM D, YYYY');
    const datepickerRanges = {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    };
    return (
      <div>
        <div className="row mb-30 stats_section">
          <div className="col d-flex align-items-stretch">
            <div className="block metrics-block">
              <div className="metrics-block__item">
                <h4>
                  <i className="fas fa-user"></i>New Players
                </h4>
                <div className="metrics-block__value">{this.state.new_users}</div>
              </div>
              <div className="metrics-block__item">
                <h4>
                  <i className="fas fa-user"></i>Active Players
                </h4>
                <div className="metrics-block__value">{this.state.online_users}</div>
              </div>
              <div className="metrics-block__item">
                <h4>
                  <i className="fas fa-user"></i>Avg Currency Total
                </h4>
                <div className="metrics-block__value">{Math.round(this.state.avg_currency)}</div>
              </div>
              <div className="metrics-block__item">
                <h4>
                  <i className="fas fa-clock"></i>Average Time
                </h4>
                <div className="metrics-block__value">{(this.state.avg_time / (60 * 60 * 1000)).toFixed(2)}</div>
              </div>
            </div>
          </div>
          <div className="col d-flex align-items-stretch flex-grow-0">
            <div className="block pie-block">
              <Donut progress={this.state.accuracy} onRender={progress => {
                return (
                  <div>
                    <div style={{height: '30px'}}>
                      <strong style={{color: 'rgb(40, 139, 230)'}}>{Math.round(progress)}%</strong>
                    </div>
                    <div style={{height: '10px'}}>
                      <span style={{color: '#73879c', fontSize: '16px', letterSpacing: 'normal'}}>correct</span>
                    </div>
                  </div>
                );
              }}
              />
              <div className="col-9 m-auto">
                <div className="select-wrapper">
                  <i className="fas fa-user"></i>
                  <Select
                    placeholder="Total"
                    options={this.optionsTotal()}
                    onChange={this.handleDropdownChangeAccuracy}
                    name='accuracy_user'
                  /> 
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row mb-30 metrics_datepicker_section">
          <div className="col-3">
            <div className="select-wrapper">
              <i className="fas fa-user"></i>
              <Select
                  placeholder="All Employees"
                  options={this.optionsTotal()}
                  onChange={this.handleDropdownChangeBadge}
                  name='badge_user'
                /> 
            </div>
          </div>
          <div className="col-5">
            <DateRangePicker
              startDate={exportDatepickerStart}
              endDate={exportDatepickerEnd}
              ranges={datepickerRanges}
              onEvent={this.handleDateRangePickerEvent}
            >
              <div id="datepicker" className="datepicker">
                <i className="far fa-calendar-alt"></i>
                <span>{exportDatepickerStartFormat + ' - ' + exportDatepickerEndFormat}</span>
              </div>
            </DateRangePicker>
          </div>
          <div className="col-2">
            <button className="btn btn-main btn-block"><i className="far fa-arrow-alt-circle-down"></i> Export</button>
          </div>
        </div>
        <div className="block icon-block">
          <div className="row">
            <div className="col-2">
              <div className="icon-item">
                <img src={icon1} alt='' />
                {this.state.badges[0]}
              </div>
            </div>
            <div className="col-2">
              <div className="icon-item">
                <img src={icon2} alt='' />
                {this.state.badges[1]}
              </div>
            </div>
            <div className="col-2">
              <div className="icon-item">
                <img src={icon3} alt='' />
                {this.state.badges[2]}
              </div>
            </div>
            <div className="col-2">
              <div className="icon-item">
                <img src={icon4} alt='' />
                {this.state.badges[12]}
              </div>
            </div>
            <div className="col-2">
              <div className="icon-item">
                <img src={icon5} alt='' />
                {this.state.badges[13]}
              </div>
            </div>
            <div className="col-2">
              <div className="icon-item">
                <img src={icon6} alt='' />
                {this.state.badges[14]}
              </div>
            </div>
            <div className="col-2">
              <div className="icon-item">
                <img src={icon7} alt='' />
                {this.state.badges[3]}/{this.state.badges[4]}/{this.state.badges[5]}
              </div>
            </div>
            <div className="col-2">
              <div className="icon-item">
                <img src={icon8} alt='' />
                {this.state.badges[6]}/{this.state.badges[7]}/{this.state.badges[8]}
              </div>
            </div>
            <div className="col-2">
              <div className="icon-item">
                <img src={icon9} alt='' />
                {this.state.badges[9]}/{this.state.badges[10]}/{this.state.badges[11]}
              </div>
            </div>
            <div className="col-2">
              <div className="icon-item">
                <img src={icon10} alt='' />
                {this.state.badges[15]}
              </div>
            </div>
            <div className="col-2">
              <div className="icon-item">
                <img src={icon11} alt='' />
                {this.state.badges[16]}
              </div>
            </div>
            <div className="col-2">
              <div className="icon-item">
                <img src={icon12} alt='' />
                {this.state.badges[17]}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    users: state.admin.users,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    getUsersRequest: () => dispatch(getUsersRequest()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(buttonStyle, regularFormsStyle)(MetricsPage));
