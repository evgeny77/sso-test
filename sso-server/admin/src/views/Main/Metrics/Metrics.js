import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';

import MetricsPage from './MetricsPage';

class Metrics extends Component {
  render() {
    return (
      <Switch>
        <Route path="/metrics" component={MetricsPage} />
      </Switch>
    );
  }
}

export default Metrics;
