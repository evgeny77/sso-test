import React from "react";
import { connect } from 'react-redux';
import { getCategoriesRequest, getQuestionsRequest } from '../../../store/actions/adminActions';
// react component used to create a calendar with events on it
import BigCalendar from "react-big-calendar";
import moment from "moment";

import ScheduleEdit from './ScheduleEdit.jsx';

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";

import buttonStyle from "assets/jss/material-dashboard-pro-react/components/buttonStyle.jsx";

const localizer = BigCalendar.momentLocalizer(moment);

class Calendar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      edit: null
    };
    this.hideEditBox = this.hideEditBox.bind(this);
  }
  componentWillMount() {
    this.props.getCategoriesRequest();
    this.props.getQuestionsRequest();
  }
  showEditBox(slotInfo) {
    this.setState({
      edit: (
        <ScheduleEdit 
          style={{ display: "block", marginTop: "-100px" }}
          data={slotInfo}
          onCancel={this.hideEditBox}
        />
      )
    })
  }
  hideEditBox() {
    this.setState({
      edit: null
    });
  }
  editQuestion = (event, element) => {
    this.showEditBox(event);
  }
  addQuestion(e) {
    var question = {
      _id: -1,
      date: e.start,
      category: '',
      color: '',
      title: '',
      answers: ['', '', '', ''],
      answer_correct: -1,
      explanation: '',
    }
    this.showEditBox(question);
  }
  eventColors(event, start, end, isSelected) {
    var backgroundColor = "event-";
    event.color
      ? (backgroundColor = backgroundColor + event.color)
      : (backgroundColor = backgroundColor + "default");
    return {
      className: backgroundColor
    };
  }

  render() {
    return (
      <div>
        {
          this.state.edit ? this.state.edit : 
            (
              <div>
                <BigCalendar
                  selectable
                  localizer={localizer}
                  events={this.props.questions}
                  defaultView="month"
                  scrollToTime={new Date(1970, 1, 1, 6)}
                  defaultDate={new Date()}
                  onSelectEvent={e => this.editQuestion(e)}
                  onSelectSlot={e => this.addQuestion(e)}
                  eventPropGetter={this.eventColors}
                />
                {/*<FullCalendar
                  header={{
                    left: 'prev,next today',
                    center: 'title',
                    right: ''
                  }}
                  dayNumberClickable = {false}
                  eventLimit = {false}
                  defaultView="dayGridMonth" 
                  events = {this.props.questions}
                  plugins={[ dayGridPlugin, interactionPlugin ]}
                  dateClick = {e => this.addQuestion(e)}
                  eventClick = {(event, element) => this.editQuestion(event, element)}
                />*/}
              </div>
            )
        }
      </div>
    );
  }
}


const mapStateToProps = state => {
  return {
    categories: state.admin.categories,
    questions: state.admin.questions,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    getCategoriesRequest: () => dispatch(getCategoriesRequest()),
    getQuestionsRequest: () => dispatch(getQuestionsRequest()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(buttonStyle)(Calendar));
