import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';

import Calendar from './Calendar.jsx';

class Schedule extends Component {
  render() {
    return (
      <Switch>
        <Route path="/schedule" component={Calendar} />
      </Switch>
    );
  }
}

export default Schedule;
