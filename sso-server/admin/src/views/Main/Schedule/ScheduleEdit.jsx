import React from "react";
import Select from 'react-select';
import { connect } from 'react-redux';
import { addQuestionRequest, editQuestionRequest } from '../../../store/actions/adminActions';

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";

// core components
import CustomInput from "../../../components/CustomInput/CustomInput.jsx";

import regularFormsStyle from "../../../assets/jss/material-dashboard-pro-react/views/regularFormsStyle";

class ScheduleEdit extends React.Component {
  state = {
    data: {
      _id: ((this.props.data._id !== undefined && this.props.data._id !== null) ? this.props.data._id : -1),
      date: ((this.props.data.date !== undefined && this.props.data.date !== null) ? this.props.data.date : ''),
      category: ((this.props.data.category !== undefined && this.props.data.category !== null) ? this.props.data.category : ''),
      color: ((this.props.data.color !== undefined && this.props.data.color !== null) ? this.props.data.color : ''),
      title: ((this.props.data.title !== undefined && this.props.data.title !== null) ? this.props.data.title : ''),
      answers: ((this.props.data.answers !== undefined && this.props.data.answers !== null) ? this.props.data.answers : ['', '', '', '']),
      answer_correct: ((this.props.data.answer_correct !== undefined && this.props.data.answer_correct !== null) ? this.props.data.answer_correct : -1),
      explanation: ((this.props.data.explanation !== undefined && this.props.data.explanation !== null) ? this.props.data.explanation : ''),
    },
    errors: {
      answers: ['', '', '', ''],
    },
  }
  handleInputChange = (e) => {
    const field = e.target.id;
    const value = e.target.value;

    this.setState((prevState) => {
      return {
          ...prevState,
          data: {
              ...prevState.data,
              [field]: value,
          },
          errors: {
            ...prevState.errors,
            [field]: ''
          }
      };
    });
  }
  handleDropdownChange = (e) => {
    const field = 'category';
    const value = e.value;
    var color = null;

    if(field !== undefined && field !== null && field !== '') {
      const category = this.props.categories.find(o => o.name === value);
      color = category.color;
    }

    this.setState((prevState) => {
      return {
          ...prevState,
          data: {
              ...prevState.data,
              [field]: value,
              color: (color !== null ? color: this.state.color),
          },
          errors: {
            ...prevState.errors,
            [field]: ''
          }
      };
    });
  }
  handleAnswerChange = (id, e) => {
    var prevAnswers = this.state.data.answers;
    var prevAnswerErrors = this.state.errors.answers;
    prevAnswers[id] = e.target.value;
    prevAnswerErrors[id] = '';
    this.setState((prevState) => {
      return {
          ...prevState,
          data: {
              ...prevState.data,
              answers: prevAnswers,
          },
          errors: {
            ...prevState.errors,
            answers: prevAnswerErrors,
          }
      };
    });
  }
  handleAnswerCorrectChange = (id, e) => {
    this.setState((prevState) => {
      return {
          ...prevState,
          data: {
              ...prevState.data,
              answer_correct: id,
          },
          errors: {
            ...prevState.errors,
            answer_correct: '',
          }
      };
    });
  }
  handleSubmit = (e) => {
    e.preventDefault();
    var hasError = false;
    if(this.state.data.category === null || this.state.data.category === '') {
      hasError = true;
      this.setState((prevState) => {
        return {
            ...prevState,
            errors: {
              ...prevState.errors,
              category: "This field is required"
            }
        };
      });
    }
    if(this.state.data.title === null || this.state.data.title === '') {
      hasError = true;
      this.setState((prevState) => {
        return {
            ...prevState,
            errors: {
              ...prevState.errors,
              title: "This field is required"
            }
        };
      });
    }
    if(this.state.data.explanation === null || this.state.data.explanation === '') {
      hasError = true;
      this.setState((prevState) => {
        return {
            ...prevState,
            errors: {
              ...prevState.errors,
              explanation: "This field is required"
            }
        };
      });
    }
    if(this.state.data.answer_correct === -1) {
      hasError = true;
      this.setState((prevState) => {
        return {
            ...prevState,
            errors: {
              ...prevState.errors,
              answer_correct: "Please select the answer"
            }
        };
      });
    }
    var answerErrors = this.state.errors.answers;
    this.state.data.answers.map((answer, idx) => {
      if(answer === null || answer === '') {
        hasError = true;
        answerErrors[idx] = 'This field is required';
      }
    })

    this.setState((prevState) => {
      return {
          ...prevState,
          errors: {
            ...prevState.errors,
            answers: answerErrors
          }
      };
    });

    if(!hasError) {
      if(this.state.data._id === -1) {
        this.props.addQuestionRequest(this.state.data).then(res => {
          if (res.errors) {
            this.setState(prevState => {
              return {
                ...prevState,
                errors: {...prevState.errors, ...res.errors}
              };
            });
          } else {
            this.props.onCancel();
          }
        })
      } else {
        this.props.editQuestionRequest(this.state.data).then(res => {
          if (res.errors) {
            this.setState(prevState => {
              return {
                ...prevState,
                errors: {...prevState.errors, ...res.errors}
              };
            });
          } else {
            this.props.onCancel();
          }
        })
      }
    }
  }
  categoryOptions() {
    let options = this.props.categories.map((prop) => {
      return {
        value: prop.name,
        label: prop.name
      }
    });
    return options;
  }
  render() {
    const category = this.props.categories.find(o => o.name === this.state.data.category);
    const defaultCategory = category ? { label: category.name, value: category.name } : null;
    return (
      <div className="event-block">
        <div className="block event-block__dialog">
          <button onClick={this.props.onCancel} className="close"></button>
          <form onSubmit={this.handleSubmit}>
            <div className="d-flex justify-content-between mb-3">
              <div className="event-label">Date: <span>{new Date(this.state.data.date).toDateString()}</span></div>
              <div className="event-category">
                <div className="select-wrapper">
                  <i className="fas fa-list"></i>
                  <Select
                    defaultValue={defaultCategory}
                    options={this.categoryOptions()}
                    onChange={this.handleDropdownChange}
                    name='category'
                    placeholder="Choose Category"
                  />
                </div>
              </div>
            </div>
            <div className="event-question mb-4">
              <label className="event-label" htmlFor="title">Question:</label>
              <textarea id="title" name="title" className="form-control" defaultValue={this.state.data.title} onChange={this.handleInputChange}></textarea>
            </div>
            <div className="event-answers mb-4">
              <label className="event-label">Answers:</label>
              <div className="event-answers__item">
                <span>#1</span>
                <label className="radio">
                  <input type="radio" checked={this.state.data.answer_correct === 0} onChange={(e) => this.handleAnswerCorrectChange(0, e)} value="0" />
                  <span></span>
                </label>
                <div className="flex-grow-1">
                  <CustomInput
                    id="answer_0"
                    name="answer_0"
                    formControlProps={{
                      fullWidth: true,
                    }}
                    onChange={(e) => this.handleAnswerChange(0, e)}
                    value={this.state.data.answers[0]}
                    error={this.state.errors.answers[0] !== undefined && this.state.errors.answers[0] !== '' && this.state.errors.answers[0] !== null}
                    helpText={this.state.errors.answers[0]}
                    style={{paddingTop: 0, margin: 0}}
                  />
                </div>
              </div>
              <div className="event-answers__item">
                <span>#2</span>
                <label className="radio">
                  <input type="radio" checked={this.state.data.answer_correct === 1} onChange={(e) => this.handleAnswerCorrectChange(1, e)} value="1" />
                  <span></span>
                </label>
                <div className="flex-grow-1">
                  <CustomInput
                    id="answer_0"
                    name="answer_0"
                    formControlProps={{
                      fullWidth: true,
                    }}
                    onChange={(e) => this.handleAnswerChange(1, e)}
                    value={this.state.data.answers[1]}
                    error={this.state.errors.answers[1] !== undefined && this.state.errors.answers[1] !== '' && this.state.errors.answers[1] !== null}
                    helpText={this.state.errors.answers[1]}
                    style={{paddingTop: 0, margin: 0}}
                  />
                </div>
              </div>
              <div className="event-answers__item">
                <span>#3</span>
                <label className="radio">
                  <input type="radio" checked={this.state.data.answer_correct === 2} onChange={(e) => this.handleAnswerCorrectChange(2, e)} value="2" />
                  <span></span>
                </label>
                <div className="flex-grow-1">
                  <CustomInput
                    id="answer_0"
                    name="answer_0"
                    formControlProps={{
                      fullWidth: true,
                    }}
                    onChange={(e) => this.handleAnswerChange(2, e)}
                    value={this.state.data.answers[2]}
                    error={this.state.errors.answers[2] !== undefined && this.state.errors.answers[2] !== '' && this.state.errors.answers[2] !== null}
                    helpText={this.state.errors.answers[2]}
                    style={{paddingTop: 0, margin: 0}}
                  />
                </div>
              </div>
              <div className="event-answers__item">
                <span>#4</span>
                <label className="radio">
                  <input type="radio" checked={this.state.data.answer_correct === 3} onChange={(e) => this.handleAnswerCorrectChange(3, e)} value="3" />
                  <span></span>
                </label>
                <div className="flex-grow-1">
                  <CustomInput
                    id="answer_0"
                    name="answer_0"
                    formControlProps={{
                      fullWidth: true,
                    }}
                    onChange={(e) => this.handleAnswerChange(3, e)}
                    value={this.state.data.answers[3]}
                    error={this.state.errors.answers[3] !== undefined && this.state.errors.answers[3] !== '' && this.state.errors.answers[3] !== null}
                    helpText={this.state.errors.answers[3]}
                    style={{paddingTop: 0, margin: 0}}
                  />
                </div>
              </div>
            </div>

            <div className="event-question mb-4">
              <CustomInput
                      labelText="Enter the explanation"
                      id="explanation"
                      name="explanation"
                      formControlProps={{
                        fullWidth: true
                      }}
                      onChange={this.handleInputChange}
                      value={this.state.data.explanation}
                      error={this.state.errors.explanation !== undefined && this.state.errors.explanation !== '' && this.state.errors.explanation !== null}
                      helpText={this.state.errors.explanation}
                    />
            </div>

            <div className="d-flex justify-content-center">
              <button className="btn btn-main" type="submit">Save and Exit</button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    categories: state.admin.categories,
    questions: state.admin.questions,
  }
}

const mapDispatchToProps = dispatch => {
  return {
      addQuestionRequest: (data) => dispatch(addQuestionRequest(data)),
      editQuestionRequest: (data) => dispatch(editQuestionRequest(data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(regularFormsStyle)(ScheduleEdit));
