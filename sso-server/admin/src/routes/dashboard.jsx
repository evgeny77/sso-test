import Metrics from '../views/Main/Metrics/Metrics';
import Schedule from '../views/Main/Schedule/Schedule';

// @material-ui/icons
import DateRangeIcon from "@material-ui/icons/DateRange";
import BarChartIcon from "@material-ui/icons/BarChart";

var dashRoutes = [
  {
    path: "/metrics",
    name: "Metrics",
    icon: BarChartIcon,
    component: Metrics
  },
  {
    path: "/schedule",
    name: "Schedule",
    icon: DateRangeIcon,
    component: Schedule
  },
  { redirect: true, path: "/", pathTo: "/metrics", name: "Metrics" },
];
export default dashRoutes;
