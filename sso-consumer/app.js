const express = require("express");
const morgan = require("morgan");
const app = express();
const engine = require("ejs-mate");
const session = require("express-session");
const axios = require("axios");

const isAuthenticated = require("./isAuthenticated");
const checkSSORedirect = require("./checkSSORedirect");

app.use(
  session({
    secret: "keyboard cat",
    resave: false,
    saveUninitialized: true
  })
);

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use(morgan("dev"));
app.engine("ejs", engine);
app.set("views", __dirname + "/views");
app.set("view engine", "ejs");
app.use(checkSSORedirect());

app.get("/", isAuthenticated, async (req, res, next) => {
  try {
    const response = await axios.get(
      'http://35.172.129.106:3010/simplesso/score?email=' + req.session.user.email,
      {
        method: 'get'
      }
    );
    var score = 0;
    if(response.data.score) {
      score = response.data.score;
    }
    res.render("index", {
      title: "SSO / AD Mini App (Consumer) for Comcast",
      name: req.session.user.name,
      email: req.session.user.email,
      score: score,
    });
  } catch (err) {
    return next(err);
  }
});

app.use((req, res, next) => {
  // catch 404 and forward to error handler
  const err = new Error("Resource Not Found");
  err.status = 404;
  next(err);
});

app.use((err, req, res, next) => {
  console.error({
    message: err.message,
    error: err
  });
  const statusCode = err.status || 500;
  let message = err.message || "Internal Server Error";

  if (statusCode === 500) {
    message = "Internal Server Error";
  }
  res.status(statusCode).json({ message });
});

module.exports = app;
